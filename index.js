const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

(async () => {
  try {
    const user = await prisma.user.create({ data: { name: "TestUser" } });

    const profile = await prisma.profile.create({
      data: {
        name: "TestProfile",
        userId: user.id,
        photo: { create: { url: "http://example.com/img" } },
      },
    });

    console.log(profile);
  } catch (e) {
    console.error(e);
  }

  await prisma.$disconnect();
})();
